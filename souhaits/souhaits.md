
# Table of Contents

1.  [Livres](#org4a67e72)
2.  [Vins:](#orge418082)
3.  [Cuisine](#org1813adc)
    1.  [Moule à manqué demontable 24cm](#orgb4430a2)



<a id="org4a67e72"></a>

# Livres

1.  [Petits jardins design](https://issuu.com/editionsulmer/docs/9782379220746_1?fr=sZjdjMjMwODk2)
    35 projets contemporains pour vivre au jardin
    Matt KEIGHTLEY - Marianne MAJERUS
    400 illustrations - 256 pages
    ISBN : 9782379220746
    Année d&rsquo;édition : 2020
    29.90 €
    
    35 projets contemporains pour vivre au jardin
    
    Matt Keightley présente dans ce livre 35 petits jardins design, aux styles très divers, allant du jardin minimaliste au jardin familial.
    
    Conçus par des paysagistes actuels de renom et par l&rsquo;auteur lui-même, tous ces jardins ont pour objectif de valoriser l&rsquo;espace extérieur en le liant à la maison, et de le transformer en une pièce à vivre supplémentaire dont on pourra profiter en toute saison. En s&rsquo;appuyant sur les remarquables photos de Marianne Majerus, l&rsquo;auteur en révèle les caractéristiques et les points forts.
    
    Que vous souhaitiez redessiner tout ou partie de votre jardin, ou que vous cherchiez juste l&rsquo;inspiration, vous trouverez dans ce livre de nombreuses idées très actuelles, des astuces et des conseils qui vous permettront de mener à bien votre projet personnel.
2.  [L&rsquo;Esprit du Japon dans nos jardins](https://www.editions-ulmer.fr/editions-ulmer/l-esprit-du-japon-dans-nos-jardins-2eme-edition-302-cl.htm)
    Ulmer
    2ème édition
    Jean-Paul PIGEAT
    160 illustrations - 168 pages
    ISBN : 9782841385768
    Année d&rsquo;édition : 2012
3.  [Farrow and Ball - Recettes couleurs](https://www.editions-eyrolles.com/Livre/9782212677805/farrow-and-ball-recettes-couleurs)
    Titre : Farrow and Ball - Recettes couleurs
    Auteur(s) : Joa Studholme
    Editeur(s) : Eyrolles
    Parution : 5 sept. 2019
    Edition : 1ère édition
    Support : aucun
    Nb de pages : 268 pages
    Format : 23,5 x 26,5
    Couverture : Cartonné
    Poids : 1568 g
    Intérieur : Quadri
    Diffusion : Geodif
    ISBN13 : 978-2-212-67780-5
    EAN13 : 9782212677805
    ISBN10 : 2-212-67780-4
    Type produit : Ouvrage
4.  Tintins
    J&rsquo;ai déja:
    -   Lotus bleu
    -   Temple du soleil
    -   Trésor de Rackham le Rouge
5.  [Sélection de livres sur les plantes d&rsquo;intérieur](https://www.conservation-nature.fr/livres-plantes/livre-plantes-interieur/)
    -   En particulier : [Urban Jungle](https://www.eyrolles.com/Loisirs/Livre/urban-jungle-9782212674507/)
        Décorer avec les plantes
        Igor Josifovic, Judith De Graaff
        (1 avis) Donner votre avis
        182 pages, parution le 07/09/2017
        24,00 €
6.  [100 styles de jardins](https://www.editions-ulmer.fr/editions-ulmer/100-styles-de-jardins-tour-du-monde-des-creations-contemporaines-les-plus-marquantes-448-cl.htm)
    Tour du monde des créations contemporaines les plus marquantes
    Emma REUSS
    600 illustrations - 432 pages
    ISBN : 9782841387571
    Année d&rsquo;édition : 2015
    35.00 €
    
    100 Styles de jardins présente 100 jardins exceptionnels, emblématiques d&rsquo;un style ou d&rsquo;un créateur.
    
    Pour chaque jardin, un focus est fait sur les points clés de la création.
    
    Mêlant les créations d&rsquo;architectes paysagistes de renommée internationale à celles d&rsquo;amateurs éclairés, ce livre constitue un formidable inventaire d&rsquo;idées et de styles qui inspirera tous les passionnés de jardins.
    \*\*
7.  [Biologie végétale (Ed. De Boeck)](https://www.deboecksuperieur.com/ouvrage/9782804181567-biologie-vegetale#description)
    Susan E Eichhorn, Ray F Evert, Peter H Raven
    Traducteur : Jules Bouharmont
    3e édition | janvier 2014 | 880 pages
    9782804181567
    
    Ouvrage moderne bien illustré, qui représente une synthèse de nos connaissances actuelles sur les plantes, leur fonctionnement, leur origine, leur évolution et leur rôle dans&#x2026; Voir la suite
8.  slow cosmétiques
9.  Atlas des relations internationales (Pascal Boniface)
10. [Agrumes résistant au froid](https://www.editions-ulmer.fr/editions-ulmer/agrumes-resistant-au-froid-a-cultiver-en-pleine-terre-820-bri-cl.htm)
    [Découverte en vidéo](https://youtu.be/JgrKL5srHjA)
    à cultiver en pleine terre
    Olivier BIGGIO - Bertrand LONDEIX
    300 illustrations - 128 pages
    Format : 17 x 24 cm
    ISBN : 9782379222054
    Année d&rsquo;édition : 2022
    Date de parution : 13 janvier 2022
    > Je souhaite être informé dès la parution de ce livre
    
    Le premier livre consacré à la culture des agrumes rustiques en pleine terre. Indispensable à tous ceux qui rêvent de cultiver des agrumes (kumquat, orangers, mandariniers, yuzu, citron&#x2026;) dans leur jardin, mais qui n&rsquo;habitent pas sur les rives de la Méditerranée.
    
    Aujourd&rsquo;hui, il existe des variétés d&rsquo;agrumes qu&rsquo;il est possible de cultiver en pleine terre quelle que soit la région de France dans laquelle on vit. C&rsquo;est une grande avancée pour les jardiniers car la culture des agrumes est bien plus facile en pleine terre qu&rsquo;en pot ou en serre : moins de maladies, de carences et de parasites, et donc moins de soins, de taille et d&rsquo;entretien.
    
    Cette culture est à la portée de tous en respectant quelques règles de base et les conseils dispensés par deux passionnés.
    
    -   Quels agrumes cultiver selon la région où l’on vit
    -   Des conseils d’achat
    -   Comment cultiver les agrumes en pleine terre
    -   Comment multiplier les agrumes
    -   Comment en prendre soin
    -   Des recettes : marmelades, confitures, pâtisserie, liqueurs…


<a id="orge418082"></a>

# Vins:

-   Un bon rhum
-   Banuyls, Maury&#x2026; ou autre!


<a id="org1813adc"></a>

# Cuisine


<a id="orgb4430a2"></a>

## [Moule à manqué demontable 24cm](https://www.debuyer.com/fr/moule-a-manque-demontable-acier-antiadhesif-1237.html)

\*\*

